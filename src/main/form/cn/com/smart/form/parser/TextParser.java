package cn.com.smart.form.parser;

import cn.com.smart.form.enums.TextDataType;
import cn.com.smart.form.enums.TextPluginType;
import cn.com.smart.form.list.AbstractFormListFieldParser;
import cn.com.smart.form.list.bean.AbstractListFieldProp;
import cn.com.smart.form.list.bean.CommonListFieldProp;
import cn.com.smart.form.list.bean.DateListFieldProp;
import cn.com.smart.form.list.bean.TextPluginListFieldProp;
import cn.com.smart.form.list.helper.FormListParseHelper;
import com.mixsmart.enums.YesNoType;
import com.mixsmart.utils.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 解析文本框
 * @author lmq
 * @version 1.0 
 * @since 1.0
 * 2015年7月4日
 */
@Component
public class TextParser extends AbstractFormListFieldParser implements IFormParser {

	protected String plugin = "text";
	
	@Override
	public String getPlugin() {
		return this.plugin;
	}
	
	@Override
	public String parse(Map<String,Object> dataMap) {
		if(null == dataMap || dataMap.size()<1) {
			return null;
		}
		boolean isHide = false;
		if(YesNoType.YES.getStrValue().equals(StringUtils.handleNull(dataMap.get("orghide")))) {
			isHide = true;
		}
		
		String value = StringUtils.handleNull(dataMap.get("value"));
		String classDefaultTag = "";
		if(StringUtils.isNotEmpty(value) && value.startsWith("${")) {
			if("${today}".equals(value)) {
				classDefaultTag = "cnoj-date-defvalue";
			} else if("${username}".equals(value)) {
				classDefaultTag = "cnoj-sysuser-defvalue";
			} else if("${deptname}".equals(value)) {
				classDefaultTag = "cnoj-sysdeptname-defvalue";
			} else if("${orgId}".equals(value)) {
                classDefaultTag = "cnoj-sysorgid-defvalue";
            } else if("${userId}".equals(value)) {
                classDefaultTag = "cnoj-sysuserid-defvalue";
            } else if("${fullName}".equals(value)) {
				classDefaultTag = "cnoj-sysfullname-defvalue";
			}else if("${uuid}".equals(value)) {
				classDefaultTag = "cnoj-sysuuid-defvalue";
			}
			value = "";
		}
		StringBuilder strBuild = new StringBuilder();
		strBuild.append("<input type=\"text\" name=\""+StringUtils.handleNull(dataMap.get("bind_table_field"))+"\" id=\""+dataMap.get("bind_table_field")+"\" data-label-name=\""+dataMap.get("title")+"\" value=\""+value+"\"  style=\""+dataMap.get("style")+"\"");
		String dataFormat = StringUtils.handleNull(dataMap.get("data_format"));
		if(!StringUtils.isEmpty(dataFormat)) {
			strBuild.append(" data-format=\""+dataFormat+"\"");
		}
		String dateFormat = StringUtils.handleNull(dataMap.get("date_format"));
		if(StringUtils.isNotEmpty(dateFormat)) {
			strBuild.append(" data-date-format=\""+dateFormat+"\"");
		}
		String relateField = StringUtils.handleNull(dataMap.get("relate_field"));
		if(StringUtils.isNotEmpty(relateField)) {
			strBuild.append(" relate-field=\""+relateField+"\"");
		}
		String relateFieldValue = StringUtils.handleNull(dataMap.get("relate_field_value"));
		if(StringUtils.isNotEmpty(relateFieldValue)) {
			strBuild.append(" relate-field-value=\""+relateFieldValue+"\"");
		}
		
		String className = StringUtils.handleNull(dataMap.get("class"))+" "+classDefaultTag;
		String orgType = StringUtils.handleNull(dataMap.get("orgtype"));
		String inputPlugin = StringUtils.handleNull(dataMap.get("input_plugin"));
		if(!StringUtils.isEmpty(inputPlugin)) {
			strBuild.append(" class=\""+className);
			if(isHide) strBuild.append(" hide ");
			strBuild.append(" "+inputPlugin+"\"");
			strBuild.append(" data-uri=\""+StringUtils.handleNull(dataMap.get("input_plugin_uri"))+"\"");
			if(TextPluginType.TEXT_PLUGIN_SELECT.getValue().equals(inputPlugin) || 
			        TextPluginType.TEXT_PLUGIN_TREE.getValue().equals(inputPlugin)) {
				if(YesNoType.YES.getStrValue().equals(StringUtils.handleNull(dataMap.get("fieldrequire"))))
					strBuild.append(" data-is-show-none=\"no\"");
			}
			if(TextPluginType.TEXT_PLUGIN_SELECT.getValue().equals(inputPlugin)) {
				if(YesNoType.YES.getStrValue().equals(StringUtils.handleNull(dataMap.get("fieldrequire"))))
					strBuild.append(" data-is-show-all=\"no\"");
			}
		} else if(!("date").equals(orgType) && !"datetime".equals(orgType) && !"time".equals(orgType) && !"text".equals(orgType)){
			strBuild.append(" class=\""+className);
			if(isHide) strBuild.append(" hide ");
			strBuild.append(" \"");
			strBuild.append(" data-format=\""+orgType+"\" ");
		} else {
			strBuild.append(" class=\""+className+" "+(isHide?"hide":"")+" \"");
		}
		String readOnly = StringUtils.handleNull(dataMap.get("is_readyonly"));
		if(YesNoType.YES.getStrValue().equals(readOnly)) {
		    strBuild.append(" readonly=\"readonly\"");
		}
		strBuild.append(" />");
		//System.out.println("strBuild.toString()::"+strBuild.toString());
		return strBuild.toString();
	}
	
	@Override
    protected AbstractListFieldProp parseListField(Map<String, Object> dataMap) {
	    String orgType = StringUtils.handleNull(dataMap.get("orgtype"));
        if(StringUtils.isEmpty(orgType)) {
            return null;
        }
        AbstractListFieldProp listFieldProp = null;
        if(TextDataType.DATE.getValue().equals(orgType) || 
                TextDataType.DATETIME.getValue().equals(orgType) || 
                TextDataType.TIME.getValue().equals(orgType)) {
            DateListFieldProp dateFieldProp = new DateListFieldProp();
            String dateFormat = StringUtils.handleNull(dataMap.get("date_format"));
            dateFieldProp.setDateFormat(dateFormat);
            dateFieldProp.setDateType(orgType);
            listFieldProp = dateFieldProp;
        } else {
            String inputPlugin = StringUtils.handleNull(dataMap.get("input_plugin"));
            if(StringUtils.isEmpty(inputPlugin)) {
                listFieldProp = new CommonListFieldProp();
            } else {
                TextPluginListFieldProp pluginFieldProp = new TextPluginListFieldProp();
                pluginFieldProp.setTextPluginType(inputPlugin);
                pluginFieldProp.setUrl(StringUtils.handleNull(dataMap.get("input_plugin_uri")));
                listFieldProp = pluginFieldProp;
            }
        }
        if(FormListParseHelper.parseCommonField(dataMap, listFieldProp)) {
           return listFieldProp;
        }
        return null;
    }
}
